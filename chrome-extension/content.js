const VERSION = chrome.runtime.getManifest().version;

const DEV_SERVER = 'http://localhost:8080';
const PROD_SERVER = 'https://mall-discount.prochac.cz';


const SERVER = PROD_SERVER;

function getPrice(...catalogNumbers) {
    return new Promise((resolve, reject) => {
        chrome.storage.sync.get(['email', 'password', 'token'], result => {

            let headers = new Headers();
            headers.append('X-Plugin-Version', VERSION);
            if (result.token) {
                headers.append('Authorization', 'Bearer ' + result.token);
            } else if (result.email && result.password) {
                headers.append('Authorization', 'Basic ' + window.btoa(result.email + ":" + result.password));
            }

            return fetch(SERVER + '/?variantId=' + catalogNumbers.join(','), {headers, redirect: 'follow'})
                .then(resp => {
                    const resp2 = resp.clone();
                    return new Promise((resolve, reject) => {
                        resp.json()
                            .then(json => resolve(json))
                            .catch(err => {
                                console.log(err);
                                resp2.text()
                                    .then(text => reject(new Error(text)));
                            });
                    });
                }).then(resp => resolve(resp)).catch(err => reject(err));
        });
    });
}

function replaceDetail() {
    const catalogNumberEl = document.querySelector('span[data-sel="catalog-number"]');
    if (!catalogNumberEl) return;

    const catalogNumber = catalogNumberEl.innerHTML;

    const priceEl = document.querySelector('b.pro-price');
    if (!priceEl) return;

    return getPrice(catalogNumber)
        .then(prices => changePrice(priceEl, prices[catalogNumber]))
        .catch(err => showError(priceEl, err));
}

async function replaceList() {
    const promises = [...document.querySelectorAll('.lst-item')].map(replaceItemPrice);
    await Promise.all(promises)
}

async function replaceItemPrice(item) {
    const priceEl = item.querySelector(".lst-product-item-price-value");
    if (!priceEl) return;

    const catalogNumber = item.id.replace('list-', '');
    await getPrice(catalogNumber)
        .then(prices => changePrice(priceEl, prices[catalogNumber]))
        .catch(err => showError(priceEl, err));
}

function changePrice(priceElement, productInfo) {
    if (productInfo.isMarketplace) {
        priceElement.innerHTML = `${czechPriceFormat(productInfo.web)}&nbsp;<font color='#00add8'>P</font>`;
        priceElement.setAttribute("title", [
                "Partnerský prodej",
                "Pro partnerské produkt není žádná sleva, protože jej neprodává Mall.cz",
            ].join("\n")
        );
        return;
    }

    const discountPercent = 100 - Math.round(productInfo.discount * 100 / productInfo.web);

    if (productInfo.discountPlus) {
        priceElement.innerHTML = `${czechPriceFormat(productInfo.discount)}&nbsp;<font color='#3d4951'>(${discountPercent}%)</font>`;
    } else {
        priceElement.innerHTML = `${czechPriceFormat(productInfo.web)}&nbsp;<font color='#3d4951'>😞</font>`;
    }
    priceElement.setAttribute("title", [
            `Běžná cena: ${czechPriceFormat(productInfo.web)}`,
            `Zaměstnanecká cena: ${czechPriceFormat(productInfo.discount)}`,
            `Procent sleva: ${discountPercent}%`,
            `Total sleva: ${czechPriceFormat(productInfo.web - productInfo.discount)}`,
        ].join("\n")
    );
}

function czechPriceFormat(price) {
    return Number(price).toLocaleString("cs-CZ", {
        style: "currency",
        currency: "CZK",
        minimumFractionDigits: 0,
        maximumFractionDigits: 0
    }).replace(/ /g, '&nbsp;');
}

function showError(priceElement, err) {
    const oldPrice = priceElement.innerHTML.trim().replace(/ /g, '&nbsp;');
    priceElement.innerHTML = `${oldPrice}&nbsp;<font color='#ffeb3b'>⚠️</font>`;
    priceElement.setAttribute("title", [
            err.message,
        ].join("\n")
    );
}

addEventListener('load', function () {
    replaceDetail();
    return replaceList();
});


let query = window.location.search;
document.addEventListener('click', function () {
    if (query === window.location.search) return;

    document.querySelector('.loader-overlay')
        .addEventListener('DOMNodeRemoved', () =>
            setTimeout(replaceList, 100), false);
    query = window.location.search;
}, false);