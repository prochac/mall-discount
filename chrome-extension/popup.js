const email = document.getElementById('email');
const password = document.getElementById('password');
const saveCredentials = document.getElementById('button');

const token = document.getElementById('token');
const tokenGen = document.getElementById('tokenGen');
const tokenSave = document.getElementById('tokenSave');

saveCredentials.onclick = () =>
    chrome.storage.sync.set({email: email.value, password: password.value}, () => {
        load();
        window.close();
    });

tokenSave.onclick = () =>
    chrome.storage.sync.set({token: token.value}, () => {
        load();
        window.close();
    });


function load() {
    return chrome.storage.sync.get(['email', 'password', 'token'], result => {
        email.value = result.email || "";
        password.value = result.password || "";
        token.value = result.token || "";
    });
}

load();