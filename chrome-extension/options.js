const email = document.getElementById('email');
const password = document.getElementById('password');
const saveCredentials = document.getElementById('button');

const token = document.getElementById('token');
const tokenExp = document.getElementById('tokenExp');
const tokenExpBool = document.getElementById('tokenExpBool');
const tokenGen = document.getElementById('tokenGen');
const tokenSave = document.getElementById('tokenSave');

saveCredentials.onclick = () =>
    chrome.storage.sync.set({email: email.value, password: password.value}, () => {
        load();
        window.close();
    });

tokenSave.onclick = () =>
    chrome.storage.sync.set({token: token.value}, () => {
        load();
        window.close();
    });

tokenGen.onclick = () => {
    const unixMs = tokenExp.valueAsNumber;
    const unixS = unixMs / 1000;
    const offsetS = new Date().getTimezoneOffset() * 60;
    const expireAt = unixS + offsetS;

    const DEV_SERVER = 'http://localhost:8080';
    const PROD_SERVER = 'https://mall-discount.prochac.cz';
    const SERVER = PROD_SERVER;

    let headers = new Headers();
    headers.append('X-Plugin-Version', chrome.runtime.getManifest().version);
    if (email.value && password.value) {
        headers.append('Authorization', 'Basic ' + window.btoa(email.value + ":" + password.value));
    }

    let url = new URL(SERVER + '/token');
    if (tokenExpBool.checked && expireAt) {
        url.search = new URLSearchParams({expireAt}).toString();
    }
    return fetch(url, {headers, redirect: 'follow'})
        .then(resp => {
            const resp2 = resp.clone();
            return resp.json()
                .then(json => {
                    token.value = json.token;
                })
                .catch(err => {
                    token.value = (err);
                    resp2.text().then(text => token.value += text);
                });
        })
};

function load() {
    return chrome.storage.sync.get(['email', 'password', 'token'], result => {
        email.value = result.email || "";
        password.value = result.password || "";
        token.value = result.token || "";
    });
}

load();