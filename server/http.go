package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/chi"
)

type Credentials struct {
	Email    string
	Password string
}

type JWTClaims struct {
	Email     string `json:"email"`
	Password  string `json:"password"`
	Nonce     []byte `json:"nonce"`
	ExpiresAt int64  `json:"exp,omitempty"`
}

func (c JWTClaims) Valid() error {
	now := time.Now().Unix()

	if c.ExpiresAt != 0 && now > c.ExpiresAt {
		delta := time.Unix(now, 0).Sub(time.Unix(c.ExpiresAt, 0))
		return jwt.ValidationError{
			Inner:  fmt.Errorf("token is expired by %v", delta),
			Errors: jwt.ValidationErrorExpired,
		}
	}

	return nil
}

type Server struct {
	AESKey      []byte
	JWTSecret   []byte
	Credentials Credentials
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	router := chi.NewRouter()
	router.Use(CORS, s.tokenMiddleware)

	router.With(s.cookieMiddleware).Get("/", s.mainHandler)
	router.Get("/token", s.tokenHandler)

	router.ServeHTTP(w, r)
}

func (s *Server) mainHandler(w http.ResponseWriter, r *http.Request) {
	cookie, ok := r.Context().Value("cookie").(*http.Cookie)
	if !ok {
		http.Error(w, "missing cookie", http.StatusInternalServerError)
		return
	}

	vv, err := variantIDs(r)
	if err != nil {
		http.Error(w, fmt.Sprintf("bad parameter: %v", err), http.StatusBadRequest)
		return
	}
	d, err := Discount(cookie, vv)
	if err != nil {
		log.Println(err)
		if errors.As(err, &ErrInvalidCookie{}) {
			log.Println("cookie removed")
			http.SetCookie(w, &http.Cookie{Name: CookieName, Value: "", Path: "/", Expires: time.Unix(0, 0)})
			// TODO add hop info
			http.Redirect(w, r, r.RequestURI, http.StatusFound)
			return
		}

		http.Error(w, fmt.Sprintf("failed to get discount from server: %v", err), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(d)
}

func (s *Server) tokenHandler(w http.ResponseWriter, r *http.Request) {
	email, password, ok := r.BasicAuth()
	if !ok {
		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	var exp int64
	if values := r.URL.Query()["expireAt"]; len(values) > 0 {
		var err error
		exp, err = strconv.ParseInt(values[0], 10, 64)
		if err != nil {
			http.Error(w, fmt.Sprintf("parse %q as number: %v", r.URL.Query().Get("expireAt"), err), http.StatusBadRequest)
			return
		}
	}

	cipheredPwd, nonce, err := Encrypt(password, s.AESKey)
	if err != nil {
		http.Error(w, fmt.Sprintf("encrypt: %v", err), http.StatusInternalServerError)
		return
	}

	claims := JWTClaims{
		Email:     email,
		Password:  cipheredPwd,
		Nonce:     nonce,
		ExpiresAt: exp,
	}

	jwtToken, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString(s.JWTSecret)
	if err != nil {
		http.Error(w, fmt.Sprintf("build token: %v", err), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(map[string]string{"token": jwtToken})
}

func (s *Server) tokenMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		auth := r.Header.Get("Authorization")

		if auth == "" {
			// TODO remove this charity
			r.SetBasicAuth(s.Credentials.Email, s.Credentials.Password)
			//http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
			//return
		}

		if strings.HasPrefix(auth, "Bearer ") {
			jwtToken := strings.TrimPrefix(auth, "Bearer ")

			var claims JWTClaims
			if _, err := jwt.ParseWithClaims(jwtToken, &claims, func(token *jwt.Token) (interface{}, error) { return s.JWTSecret, nil }); err != nil {
				http.Error(w, fmt.Sprintf("bad token: %v", err), http.StatusUnauthorized)
				return
			}

			password, err := Decrypt(claims.Password, claims.Nonce, s.AESKey)
			if err != nil {
				http.Error(w, fmt.Sprintf("decript token: %v", err), http.StatusInternalServerError)
				return
			}

			r.SetBasicAuth(claims.Email, password)
		}

		h.ServeHTTP(w, r)
	})

}

func (s *Server) cookieMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cookie, err := r.Cookie(CookieName)
		if err != nil {
			email, password, ok := r.BasicAuth()
			if !ok {
				http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
				return
			}
			c, err := Login(r.Context(), email, password)
			if err != nil {
				http.Error(w, fmt.Sprintf("failed to login to remote server: %v", err), http.StatusInternalServerError)
				return
			}
			cookie = c
		}

		http.SetCookie(w, cookie)
		ctx := context.WithValue(r.Context(), "cookie", cookie)
		h.ServeHTTP(w, r.WithContext(ctx))
	})
}

func CORS(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, OPTIONS")
		if r.Method != http.MethodOptions {
			h.ServeHTTP(w, r)
		}
	})
}

func variantIDs(r *http.Request) ([]VariantID, error) {
	q := r.URL.Query().Get("variantId")
	if q == "" {
		return nil, fmt.Errorf("missign 'variantId' parameter")
	}

	sl := strings.Split(q, ",")
	vv := make([]VariantID, len(sl))
	for i, s := range sl {
		v, err := strconv.Atoi(s)
		if err != nil {
			return nil, fmt.Errorf("vaiantId must be number: %w", err)
		}
		vv[i] = VariantID(v)
	}

	return vv, nil
}
