package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/hex"
	"io"
)

const gcmStandardNonceSize = 12

func Encrypt(text string, key []byte) (string, []byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", nil, err
	}

	nonce := make([]byte, gcmStandardNonceSize)
	if _, err := io.ReadFull(rand.Reader, nonce); err != nil {
		panic(err.Error())
	}

	aesgcm, err := cipher.NewGCMWithNonceSize(block, gcmStandardNonceSize)
	if err != nil {
		return "", nil, err
	}

	ciphertext := aesgcm.Seal(nil, nonce, []byte(text), nil)

	return hex.EncodeToString(ciphertext), nonce, nil
}

func Decrypt(ciphertext string, nonce []byte, key []byte) (string, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	aesgcm, err := cipher.NewGCMWithNonceSize(block, gcmStandardNonceSize)
	if err != nil {
		return "", err
	}

	unhexed, err := hex.DecodeString(ciphertext)
	if err != nil {
		return "", err
	}

	text, err := aesgcm.Open(nil, nonce, unhexed, nil)
	if err != nil {
		return "", err
	}

	return string(text), nil
}
