package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/chromedp/cdproto/network"
	"github.com/chromedp/chromedp"
)

const CookieName = "PHPSESSID"

func Login(ctx context.Context, email, password string) (*http.Cookie, error) {
	ctx, cancel := chromedp.NewContext(ctx, chromedp.WithLogf(log.Printf))
	defer cancel()

	var cookie http.Cookie
	if err := chromedp.Run(ctx,
		chromedp.Navigate("https://zbozi.nrholding.com/auth"),
		chromedp.WaitVisible(`input#userNameInput`),
		chromedp.WaitVisible(`input#passwordInput`),
		chromedp.SendKeys("input#userNameInput", email),
		chromedp.SendKeys("input#passwordInput", password),
		chromedp.Submit("input#passwordInput"),
		// wait for login
		chromedp.WaitNotPresent("script#searchForm"),
		// LOL don't remove this
		chromedp.CaptureScreenshot(new([]byte)),
		chromedp.ActionFunc(func(ctx context.Context) error {
			cookies, err := network.GetAllCookies().Do(ctx)
			if err != nil {
				return err
			}

			for _, c := range cookies {
				if c.Name == CookieName {
					cookie = http.Cookie{
						Name:    c.Name,
						Value:   c.Value,
						Expires: time.Unix(int64(c.Expires), 0),
					}
					return nil
				}
			}

			return fmt.Errorf("cookie not found")
		}),
	); err != nil {
		return nil, err
	}

	return &cookie, nil
}
