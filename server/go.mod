module chromedp

go 1.13

require (
	github.com/chromedp/cdproto v0.0.0-20190914222359-8fce60818865
	github.com/chromedp/chromedp v0.4.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v4.0.2+incompatible
	golang.org/x/net v0.0.0-20190916140828-c8589233b77d
)
