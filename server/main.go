package main

import (
	"crypto/aes"
	"log"
	"net/http"
	"os"
)

func main() {
	jwtSecret, ok := os.LookupEnv("JWT_SECRET")
	if !ok || jwtSecret == "" {
		log.Fatal("missing or empty SECRET_KEY variable")
	}
	secretKey, ok := os.LookupEnv("AES_SECRET")
	if !ok || secretKey == "" {
		log.Fatal("missing or empty SECRET_KEY variable")
	}
	switch k := len([]byte(secretKey)); k {
	default:
		log.Fatalf("Invalid SECRET_KEY variable: %v", aes.KeySizeError(k))
	case 16, 24, 32:
		break
	}

	email, ok := os.LookupEnv("MALL_EMAIL")
	if !ok || email == "" {
		log.Fatal("missing or empty MALL_EMAIL variable")
	}

	password, ok := os.LookupEnv("MALL_PASSWORD")
	if !ok || password == "" {
		log.Fatal("missing or empty MALL_PASSWORD variable")
	}

	credentials := Credentials{
		Email:    email,
		Password: password,
	}

	server := &Server{
		JWTSecret:   []byte(jwtSecret),
		AESKey:      []byte(secretKey),
		Credentials: credentials,
	}

	log.Fatal(http.ListenAndServe("0.0.0.0:8080", server))
}
