package main

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strconv"

	"golang.org/x/net/publicsuffix"
)

type ErrInvalidCookie struct {
	Err error
}

func (e ErrInvalidCookie) Error() string {
	return e.Err.Error()
}

type ErrSAP struct {
	Err error
}

func (e ErrSAP) Error() string {
	return e.Err.Error()
}

type VariantID int

func (v VariantID) String() string {
	return strconv.Itoa(int(v))
}

func variantIDsToString(ids []VariantID) string {
	var buffer bytes.Buffer
	for i, id := range ids {
		buffer.WriteString(id.String())

		if i == len(ids)-1 {
			continue
		}
		buffer.WriteString(",")
	}
	return buffer.String()
}

type DiscountResponse struct {
	Data         map[VariantID]Variant `json:"data"`
	Error        bool                  `json:"error"`
	ErrorMessage string                `json:"errorMessage"`
}

type Variant struct {
	Web                  float64 `json:"web"`
	Sap                  float64 `json:"sap"`
	SapDPH               float64 `json:"sapDPH"`
	Discount             float64 `json:"discount"`
	DiscountPlus         bool    `json:"discountPlus"`
	DiscountPlusButSmall bool    `json:"discountPlusButSmall"`
	DiscountMinusWeb     float64 `json:"discountMinusWeb"`
	SavePercentInt       float64 `json:"savePercentInt"`
	IsMarketplace        bool    `json:"isMarketplace"`
	IsBlocked            bool    `json:"isBlocked"`
}

func Discount(cookie *http.Cookie, ids []VariantID) (map[VariantID]Variant, error) {
	jar, err := cookiejar.New(&cookiejar.Options{
		PublicSuffixList: publicsuffix.List,
	})
	if err != nil {
		return nil, fmt.Errorf("failed to create cookiejar: %w", err)
	}

	jar.SetCookies(&url.URL{
		Scheme: "https",
		Host:   "zbozi.nrholding.com",
	}, []*http.Cookie{
		{
			Name:  cookie.Name,
			Value: cookie.Value,
		},
	})

	transport := http.DefaultTransport.(*http.Transport)
	transport.TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	client := http.Client{
		Jar:       jar,
		Transport: transport,
	}
	u := fmt.Sprintf("https://zbozi.nrholding.com/json/get-discount?%s", url.Values{
		"shopId":    {"CZ10MA"},
		"variantId": {variantIDsToString(ids)},
	}.Encode())

	//log.Println("Cookie:", cookie)
	//log.Println("URL:", u)
	//log.Printf("curl -v --cookie \"%s=%s\" \"%s\"\n", cookie.Name, cookie.Value, u)

	resp, err := client.Get(u)
	if err != nil {
		return nil, fmt.Errorf("request faield: %w", err)
	}
	defer resp.Body.Close()

	var dr DiscountResponse
	if err := json.NewDecoder(resp.Body).Decode(&dr); err != nil {
		return nil, fmt.Errorf("failed to decode response body: %w", err)
	}
	if dr.Error {
		switch dr.ErrorMessage {
		case "You don't have permission for search price.":
			return nil, ErrInvalidCookie{Err: errors.New(dr.ErrorMessage)}
		case "sapPriceError":
			return nil, ErrSAP{Err: errors.New(dr.ErrorMessage)}
		default:
			return nil, errors.New(dr.ErrorMessage)
		}
	}

	return dr.Data, nil
}
